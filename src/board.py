import serial


class Board:
    def __init__(self, port, baud_rate, frame):
        self.port = port
        self.baud_rate = baud_rate
        self.master_frame = frame
        self.serial = serial.Serial(self.port, self.baud_rate)

    def get_data(self):
        data = self.serial.readline().decode().rstrip()
        if data:
            parts = data.split(';')
            temperature, pH, ppm = (-1, -1, -1)
            if data.startswith('T'):
                temperature = float(parts[0].split(':')[1])
                pH = float(parts[1].split(':')[1])
                ppm = float(parts[2].split(':')[1])

            return temperature, pH, ppm
