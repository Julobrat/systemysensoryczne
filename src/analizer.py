class Analizer:
    @staticmethod
    def analize_ph(ph: float) -> (int, str):
        if not ph:
            return None, None

        score, message = None, None

        if ph < 5.5:
            score = -10
            message = "Very low pH, water too acidic, might be contaminated and make you sick, corrosive for metals"
        if 5.5 <= ph < 6.5:
            score = -5
            message = "Low pH, water is acidic, might make you sick, could be slightly contaminated."
        if 6.5 <= ph < 8.5:
            score = 10
            message = "Good pH, water is neutral, safe to drink."
        if 8.5 <= ph < 9.5:
            score = 5
            message = "High pH, water is slightly alkaline, could be good for your health in certain conditions."
        if 9.5 <= ph:
            score = -5
            message = "Very high pH, water too alkaline, might be contaminated and make you sick."

        return score, message

    @staticmethod
    def analize_ppm(ppm: float) -> (int, str):
        if not ppm:
            return None, None

        score, message = None, None

        if ppm < 150:
            score = 10
            message = "Very low PPM, sater is very pure, excellent to drink."
        if 150 <= ppm < 250:
            score = 5
            message = "Low PPM, water is pure, good for drinking."
        if 250 <= ppm < 300:
            score = 0
            message = "Decent PPM, water is fairly pure, safe to drink."
        if 300 <= ppm < 500:
            score = -5
            message = "High PPM, water is slightly contaminated, might be not good for your health."
        if 500 <= ppm:
            score = -10
            message = "Very high PPM, water is contaminated, shouldn't be used for drinking."

        return score, message

    @staticmethod
    def predict_water_quality(ph: float, ppm: float) -> (str, str, str):
        score, message = None, None

        ph_score, ph_message = Analizer.analize_ph(ph)
        ppm_score, ppm_message = Analizer.analize_ppm(ppm)
        if ph_score is None or ppm_score is None:
            return '', '', ''

        score = ph_score + ppm_score

        if score <= -15:
            message = "Water quality is very bad, don't drink it."
        if -15 < score <= -5:
            message = "Water quality is weak, better don't drink it, you can wash something"
        if -5 < score <= 0:
            message = "Water quality is acceptable, but not good."
        if 0 < score <= 5:
            message = "Water quality is pretty good, you can water flowers"
        if 5 < score <= 15:
            message = "Water quality is great, you can safely drink it."

        return ph_message, ppm_message, message
