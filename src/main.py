from board import Board
from plots import Plots
from sensor import Sensor
from window import Window

sensors = [
    Sensor(Sensor.SensorType.temperature, 'Time', 'Temperature', (0, 40)),
    Sensor(Sensor.SensorType.ph, 'Time', 'pH', (0, 14)),
    Sensor(Sensor.SensorType.ppm, 'Time', 'PPM', (0, 1000))
]

if __name__ == '__main__':
    window = Window(get_data_cooldown=1000, zoomed=True)

    plots = Plots(sensors, window.first_frame)
    window.add_plots(plots)

    try:
        window.board = Board("/dev/ttyACM0", 9600, window.third_frame)
        print('Board connected')
    except:
        print('Board not connected, using fake data')
    window.initialize_elements()

    window.run()
