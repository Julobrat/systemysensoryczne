import tkinter as tk

from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from sensor import Sensor


class Plots:
    def __init__(self, sensors: list[Sensor], frame):
        self.sensors: list[Sensor] = sensors
        self.master_frame = frame

        self.frame = tk.Frame(self.master_frame)
        self.frame.grid(row=1, column=0, columnspan=3)

        self.plots: list[Plot] = [Plot(sensor, self.frame, size=(7, 7), dpi=60) for sensor in self.sensors]

        self._create_checkbuttons()

    def clear_plots(self):
        for plot in self.plots:
            plot.reset()

    def update_max_data_points(self):
        for plot in self.plots:
            plot.update_max_data_points()

    def _create_checkbuttons(self):
        for i, plot in enumerate(self.plots):
            # max data points
            tk.Entry(
                self.frame, textvariable=plot.max_data_points, font=('Arial', 20), width=5
            ).grid(row=3, column=i, padx=10, pady=10)

            # hide plots checkbutton
            tk.Checkbutton(
                self.frame, text=f'Hide {plot.sensor.sensor_type.value}', variable=plot.hidden, onvalue=True, offvalue=False, command=lambda plot=plot: plot.switch()
            ).grid(row=4, column=i, padx=10, pady=10)

            # uncertainty of measurement
            tk.Checkbutton(
                self.frame, text=f'Uncertainty {plot.sensor.sensor_type.value}', variable=plot.uncertainty, onvalue=True, offvalue=False
            ).grid(row=5, column=i, padx=10, pady=10)


class Plot:
    number_of_plots: int = 0

    def __init__(self, sensor: Sensor, frame, size, dpi):
        self.sensor: Sensor = sensor

        self.master_frame = frame
        self.figsize = size
        self.dpi = dpi
        self.hidden = tk.BooleanVar(value=False)
        self.max_data_points = tk.StringVar(value='25')
        self.uncertainty = tk.BooleanVar(value=False)

        self.plot_fig = None
        self.plot_ax = None
        self.plot_frame = None

        self.plot_index: int = Plot.number_of_plots
        Plot.number_of_plots += 1

    def plot(self):
        if not self.plot_fig:
            self.plot_fig = Figure(figsize=self.figsize, dpi=self.dpi)

        if not self.plot_ax:
            self.plot_ax = self.plot_fig.add_subplot(111)
        else:
            self.plot_ax.clear()

        # main measurment
        self.plot_ax.plot(self.sensor.xdata, self.sensor.ydata, color='black', linewidth=2, markersize=4, alpha=1)
        self.plot_ax.set_title(self.sensor.sensor_type.value)
        self.plot_ax.set_xlabel(self.sensor.xlabel)
        self.plot_ax.set_ylabel(self.sensor.ylabel)
        self.plot_ax.xaxis.set_major_locator(plt.MaxNLocator(integer=True))
        self.plot_ax.set_ylim(*self.sensor.data_lims)
        self.plot_ax.title.set_fontsize(20)
        self.plot_ax.xaxis.label.set_fontsize(15)
        self.plot_ax.yaxis.label.set_fontsize(15)

        # measurement uncertainty
        if self.uncertainty.get():
            match self.sensor.sensor_type:
                case Sensor.SensorType.temperature:
                    delta = 0.5
                case Sensor.SensorType.ph:
                    delta = 0.1
                case Sensor.SensorType.ppm:
                    delta = 10

            delta_plus = [y + delta for y in self.sensor.ydata]
            delta_minus = [y - delta for y in self.sensor.ydata]
            self.plot_ax.plot(self.sensor.xdata, delta_plus, 'r--', linewidth=2, markersize=1, alpha=0.5)
            self.plot_ax.plot(self.sensor.xdata, delta_minus, 'r--', linewidth=2, markersize=1, alpha=0.5)

        if not self.plot_frame:
            self.plot_frame = FigureCanvasTkAgg(self.plot_fig, master=self.master_frame)
            self.plot_frame.get_tk_widget().grid(row=1, column=self.plot_index, padx=10, pady=10)

        self.plot_frame.draw()

    def switch(self):
        frame = self.plot_frame.get_tk_widget()
        if frame.winfo_manager():
            frame.grid_forget()
        else:
            frame.grid(row=1, column=self.plot_index, padx=10, pady=10)

    def update(self, data_from_board):
        self.sensor.update_data(data_from_board)
        self.plot()

    def reset(self):
        self.sensor.reset_data()
        self.plot()

    def update_max_data_points(self):
        max_data_points = self.sensor.max_data_points
        input_value = self.max_data_points.get()

        try:
            if 1 <= int(input_value) <= 1000:
                max_data_points = int(input_value)
            else:
                raise Exception("Wrong value")
        except:
            print(f"Value: {input_value} is not permitted. Type a number from 1 to 1000")

        self.sensor.max_data_points = max_data_points
