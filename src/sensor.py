from enum import Enum

import numpy as np


class Sensor:
    class SensorType(Enum):
        temperature = 'Temperature'
        ph = 'pH'
        ppm = 'PPM'

    def __init__(self, sensor_type: SensorType, xlabel: str, ylabel: str, data_lims: (float, float), max_data_points: int = 25):
        self.sensor_type: Sensor.SensorType = sensor_type
        self.xlabel: str = xlabel
        self.ylabel: str = ylabel
        self.xdata: list = []
        self.ydata: list = []
        self.max_data_points: int = max_data_points
        self.data_lims: (float, float) = data_lims

    def update_data(self, data_from_board):
        match self.sensor_type:
            case Sensor.SensorType.temperature:
                data_point = data_from_board[0]
            case Sensor.SensorType.ph:
                data_point = data_from_board[1]
            case Sensor.SensorType.ppm:
                data_point = data_from_board[2]
            case _:
                raise ValueError('Wrong type of sensor')

        self.xdata.append(self.xdata[-1] + 1 if self.xdata else 1)
        self.ydata.append(data_point)
        while len(self.ydata) > self.max_data_points:
            self.xdata.pop(0)
            self.ydata.pop(0)

    def reset_data(self):
        self.xdata = []
        self.ydata = []
