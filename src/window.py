import tkinter as tk

import numpy as np
import pandas as pd
from pandastable import Table, TableModel

from plots import Plot, Plots
from utils import colors
from analizer import Analizer


class Window:
    def __init__(self, get_data_cooldown: int = 1000, zoomed: bool = False, keep_getting_data: bool = True):
        # window settings
        self.root = tk.Tk()
        self.root.title('Sensors')
        self.root.geometry('1400x600')
        self.root.state('zoomed') if zoomed else None

        # meta settings
        self.get_data_cooldown: int = get_data_cooldown
        self.keep_getting_data: tk.BooleanVar = tk.BooleanVar(value=keep_getting_data)

        # base frame
        self.base_frame = tk.Frame(self.root)
        self.base_frame.pack(fill=tk.BOTH, expand=True)

        # canvas
        self.canvas = tk.Canvas(self.base_frame)
        self.canvas.config(bg=colors.bg1)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        # add horizontal scrollbar
        self.horizontal_scrollbar = tk.Scrollbar(self.base_frame, orient=tk.VERTICAL, command=self.canvas.yview)
        self.horizontal_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.canvas.config(yscrollcommand=self.horizontal_scrollbar.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox('all')))
        self.canvas.bind_all("<MouseWheel>", lambda event: self.canvas.yview_scroll(int(-1 * (event.delta / 120)), "units"))

        # another frame inside canvas
        self.canvas_frame = tk.Frame(self.canvas, bg=colors.bg2)
        self.canvas.create_window(0, 0, window=self.canvas_frame)

        # first frame for plots
        self.first_frame = tk.Frame(self.canvas_frame)
        self.first_frame.config(bg=colors.bg2)
        self.first_frame.pack(padx=10, pady=10)

        # fourth frame and labels for analysis
        self.fourth_frame = tk.Frame(self.canvas_frame)
        self.fourth_frame.config(bg=colors.bg2)
        self.fourth_frame.pack(padx=10, pady=10)
        self.analysis_ph_label = tk.Label(self.fourth_frame, text='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', font=('Arial', 20), bg=colors.bg2, fg=colors.fg)
        self.analysis_ph_label.grid(row=0, column=0, padx=10, pady=10)
        self.analysis_ppm_label = tk.Label(self.fourth_frame, text='bbbbbbbbbbb', font=('Arial', 20), bg=colors.bg2, fg=colors.fg)
        self.analysis_ppm_label.grid(row=1, column=0, padx=10, pady=10)
        self.analysis_final_label = tk.Label(self.fourth_frame, text='cccccccc', font=('Arial', 20), bg=colors.bg2, fg=colors.fg)
        self.analysis_final_label.grid(row=2, column=0, padx=10, pady=10)

        # second and third frame for pandas data
        self.second_frame = tk.Frame(self.canvas_frame)
        self.second_frame.config(bg=colors.bg2)
        self.second_frame.pack(padx=10, pady=10)
        self.pt = Table(self.second_frame, dataframe=pd.DataFrame([]), showtoolbar=True, showstatusbar=True)
        self.pt.show()

        self.third_frame = tk.Frame(self.canvas_frame)
        self.third_frame.config(bg=colors.bg2)
        self.third_frame.pack(padx=10, pady=10)



        # fields for the future
        self.label = None
        self.update_max_data_points_button = None
        self.reset_button = None
        self.keep_getting_data_checkbutton = None
        self.plots_frame = None
        self.plots: list[Plot] = []
        self.board = None
        self.data_from_board = [0, 0, 0]

    def add_plots(self, plots_frame: Plots):
        self.plots_frame = plots_frame
        self.plots = plots_frame.plots

    def initialize_elements(self):
        self.label = tk.Label(self.first_frame, text='Data from sensors', font=('Arial', 20), bg=colors.bg2, fg=colors.fg)
        self.label.grid(row=0, column=0, columnspan=3, padx=10, pady=10)

        self.update_max_data_points_button = tk.Button(self.first_frame, text='Update max data points', font=('Arial', 20), bg=colors.bg2, fg=colors.fg, command=self.plots_frame.update_max_data_points)
        self.update_max_data_points_button.grid(row=2, column=0, padx=10, pady=10)

        self.reset_button = tk.Button(self.first_frame, text='Reset data', font=('Arial', 20), bg=colors.bg2, fg=colors.fg, command=self.plots_frame.clear_plots)
        self.reset_button.grid(row=2, column=1, padx=10, pady=10)

        self.keep_getting_data_checkbutton = tk.Checkbutton(self.first_frame, text='Keep getting data', font=('Arial', 20), variable=self.keep_getting_data, onvalue=True, offvalue=False)
        self.keep_getting_data_checkbutton.grid(row=2, column=2, padx=10, pady=10)

    # background tasks
    def run_background_tasks(self):
        self._update_data()
        self._update_plots()
        self._update_dataframe()
        self._update_analysis()

    def _update_plots(self):
        if self.keep_getting_data.get():
            for plot in self.plots:
                plot.update(self.data_from_board)
        self.first_frame.after(self.get_data_cooldown, self._update_plots)

    def _update_dataframe(self):
        if self.keep_getting_data.get():
            max_length = max([len(plot.sensor.ydata) for plot in self.plots])
            data = {}
            for plot in self.plots:
                if len(plot.sensor.ydata) == max_length:
                    data[plot.sensor.sensor_type.value] = plot.sensor.ydata
                else:
                    data[plot.sensor.sensor_type.value] = [None] * (max_length - len(plot.sensor.ydata)) + plot.sensor.ydata

            self.pt.updateModel(TableModel(pd.DataFrame(data)))
            self.pt.redraw()
        self.second_frame.after(self.get_data_cooldown, self._update_dataframe)

    def _update_data(self):
        if self.keep_getting_data.get():
            if self.board is not None:
                temperature, pH, ppm = self.board.get_data()
            else:
                temperature, pH, ppm = np.random.normal(25, 1), np.random.normal(7, 0.1), np.random.normal(500, 11)

            self.data_from_board[0] = temperature if temperature != -1 else self.data_from_board[0]
            self.data_from_board[1] = pH if pH != -1 else self.data_from_board[1]
            self.data_from_board[2] = ppm if ppm != -1 else self.data_from_board[2]

        self.third_frame.after(self.get_data_cooldown, self._update_data)


    def _update_analysis(self):
        if self.keep_getting_data.get():
            ph_msg, ppm_msg, final_msg = Analizer.predict_water_quality(self.data_from_board[1], self.data_from_board[2])

            self.analysis_ph_label.config(text=ph_msg)
            self.analysis_ppm_label.config(text=ppm_msg)
            self.analysis_final_label.config(text=final_msg)
        self.fourth_frame.after(self.get_data_cooldown, self._update_analysis)

    def run(self):
        self.run_background_tasks()
        self.root.mainloop()
