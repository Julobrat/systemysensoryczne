from dataclasses import dataclass


@dataclass
class Colors:
    bg1: str
    bg2: str
    fg: str


colors = Colors('#333333', '#555555', '#ffffff')
