/*
 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
|                                                 |
| System moinitorowania parametrów wody           |
| Inteligentne systemy sensoryczne                |
|                                                 |
| Julian Górski, Dominik Kikla, Marcin Głodziak   |
| Informatyka i Systemy Inteligentne, III rok     |
| 2023                                            |    
|_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|

*/

#include <EEPROM.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "GravityTDS.h"

#define pHPin A0
#define TdsSensorPin A1
#define TemperaturePin A2

#define Offset 0.80            //pH deviation compensate

GravityTDS gravityTds;
OneWire oneWire(TemperaturePin);
DallasTemperature sensors(&oneWire);

float temperature = 25;
float tdsValue = 0;
float pH = 0;


void setup() {
  InitTds();
  sensors.begin();
  
  Serial.begin(9600);
}

void loop() {
  temperature = ReadTemperature();
  tdsValue = ReadTds(temperature);
  pH = ReadpH();

  SendData(temperature, pH, tdsValue);
  
  delay(1000);
}


void InitTds()
{    
  gravityTds.setPin(TdsSensorPin);
  gravityTds.setAref(3.3);  // voltage
  gravityTds.setAdcRange(1024);  //resolution: 1024 for 10bit ADC;4096 for 12bit ADC
  gravityTds.begin();
}

float ReadTemperature()
{
  float temp;
  
  sensors.requestTemperatures();
  temp = sensors.getTempCByIndex(0);
  
  return temp;
}

float ReadTds(float temperature)
{
  float tds;
  
  gravityTds.setTemperature(temperature);  // set the temperature and execute temperature compensation
  gravityTds.update();  //sample and calculate
  
  tds = gravityTds.getTdsValue();
  
  return tds;
}

float ReadpH()
{
  float sensorValue = analogRead(pHPin);
  float voltage = sensorValue * 5.0 / 1024;
  float pH = 3.5*voltage + Offset;

  return pH;
  
}

void SendData(float temperature, float pH, float tds)
{
  Serial.print("Temp:");
  Serial.print(temperature);
  Serial.print(";pH:");
  Serial.print(pH);
  Serial.print(";ppm:");
  Serial.println(tds);
}
